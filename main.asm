%include "colon.inc"
%include "words.inc"
%include "lib.inc"
%include "dict.inc"
%define buf_size 255
 
global _start
 
 
 
section .rodata
hello_message:    db "Enter key: ", 0
value:    db "Value is: ", 0
size_error: db "Error: max symb number is 255", 0xA, 0
mane_error: db "No such key", 0xA, 0
 
section .bss
buf: times buf_size db 0
;
section .text
 
 
_start:
    mov rdi, hello_message
    call print_string
    mov rdi, buf
    mov rsi, buf_size
    call read_word
    test rax, rax
    je .overflow
    mov rsi, POINT
    mov rdi, rax
    push rdx
    call find_word
    pop rdx
    test rax, rax
    je .word_error
    add rax, 8
    mov rdi, rax
    push rdi
    mov rdi, value
    push rdx
    call print_string
    pop rdx
    pop rdi
    add rdi, 1
    add rdi, rdx
    call print_string
    call print_newline
    jmp .end
.word_error:
    mov rdi, name_error
    call print_error
    jmp .end
.overflow:
    mov rdi, size_error
    call print_error
    jmp .end
.end:
call exit
