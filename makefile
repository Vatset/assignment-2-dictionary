ASM=nasm
ASMFLAGS=-f elf64
 
lab: lab.o lib.o dict.o
	ld -o $@ $+
 
main.o: main.asm colon.inc lib.inc words.inc dict.inc
	$(ASM) $(ASMFLAGS) -o $@ $<
 
dict.o: dict.asm lib.inc
	$(ASM) $(ASMFLAGS) -o $@ $<
 
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
 
 
 
clean:
	rm -rf *.o lab
