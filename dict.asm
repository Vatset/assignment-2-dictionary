section .text
global find_word
extern string_equals
 
find_word:
	xor rax, rax
.loop:
	test rsi, rsi
	jz .notfound
	push rdi
	push rsi
	add rsi, 8
	call string_equals
	pop rsi
	pop rdi
	test rax, rax
	jnz .found
	mov rsi, [rsi]
	jmp .loop
.found:
	mov rax, rsi
.notfound:
	xor rax,rax
ret
