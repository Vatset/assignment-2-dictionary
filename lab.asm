section .text
global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
%define EXIT_SYSCALL 60 
 
; Принимает код возврата и завершает текущий процесс
exit:
   mov rax,EXIT_SYSCALL
   syscall 
 
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
   cmp byte [rdi+rax],0
   je .end
   inc rax
   jmp .loop

.end:
   ret
 
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
   push rdi
   call string_length
	mov rsi,rdi
	mov rdx,rax
	mov rax,1
	mov rdi,1
	syscall
	pop rdi
	ret
 
print_error:
	call string_length
	mov rsi, rdi
	mov rdx, rax
	mov rax, 1
	mov rdi, 2
	syscall
	ret
; Принимает код символа и выводит его в stdout
print_char:
   push rdi
	mov rdi,rsp
	call print_string
	pop rdi
	ret 
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
  mov rdi,`\n`
   jmp print_char
   
 
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
   mov rax,rdi
	mov rdi,rsp
	push 0
	sub rsp,16
	dec rdi
	mov r8,10
.loop:
	xor rdx,rdx
	div r8
	or rdx,48
	dec rdi
	mov [rdi],dl
	test rax,rax
	jnz .loop
	call print_string
	add rsp,24
   ret
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi,rdi
	jns print_uint
	push rdi
	mov rdi,'-'
	call print_char
	pop rdi
	neg rdi
	jmp print_uint
  	
 
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	test rax,rax
   .loop:
	mov al,[rdi]
	cmp al,[rsi]
	jne .zero
	cmp rax,0x00
	je .one
	inc rdi
	inc rsi
	jmp .loop
    .zero:
	xor rax,rax
	jmp .exit
    .one:
	mov rax,1

  .exit:
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax,rax
    push 0
	xor rdi,rdi
	mov rsi,rsp
	mov rdx,1
	syscall
	pop rax
   ret
 
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
 
read_word:
   push rdi
   push rsi
   push rdi
	.read:
	    call read_char
		pop rdi
		pop rsi
	    cmp rax, 0xA
	    je .read
	    cmp rax,0x20
	    je .read
	    cmp rax,0x9
	    je .read
	.loop:
	  pop rdi
	  pop rsi   
	  test rsi,rsi
	  je .fail
	  mov [rdi],al
	  test rax,rax
	  je .success
	  cmp rax, 0xA
	  je .success
	  cmp rax, 0x20
	  je .success 
	  cmp rax,0x9
	  je .success
	  inc rdi
	  dec rsi
	  push rsi
	  push rdi
	  call read_char
	  pop rdi 
	  pop rsi
	  jmp .loop
	.fail:
	  pop rax
	  xor rax,rax
	  ret
	.success:
	  mov byte [rdi],0
	  mov rdi, [rsp]
	  call string_length
	  mov rdx,rax
	  pop rax
	  ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx,rdx
    xor rcx,rcx
    xor rsi,rsi
    mov r9,10
.loop:
	mov cl, byte [rdi+rsi]
	xor cl,'0'
	cmp cl, 9
	ja .exit
	mul r9
	add rax,rcx
	inc rsi
	jmp .loop
.exit:
	mov rdx,rsi
	ret
    
 
 
 
 
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte [rdi],'-'
	jne parse_uint
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
  call string_length
	cmp rax,rdx
	jae .fail
.loop:
	mov dl,byte[rdi]
	mov byte [rsi], dl
	inc rdi
	inc rsi
	test dl,dl
	jnz .loop
	ret
.fail:
	xor rax,rax
   	ret
